#
#  Be sure to run `pod spec lint HireplaceTok.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see https://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |spec|

  spec.platform = :ios
  spec.ios.deployment_target = '12.1'
  spec.name         = "HireplaceTok"
  spec.version      = "0.1.0"
  spec.summary      = "A short description of HireplaceTok."

  spec.homepage     = "http://EXAMPLE/HireplaceTok"
  # spec.screenshots  = "www.example.com/screenshots_1.gif", "www.example.com/screenshots_2.gif"


  spec.license      = "MIT (example)"
  # spec.license      = { :type => "MIT", :file => "FILE_LICENSE" }

  spec.author             = { "Mike Tran" => "mike.tran@abcdefhighiklab.com" }

  spec.source       = { :git => "https://bitbucket.org/hireplace/hireplacetok.git", :tag => "#{spec.version}" }


  spec.source_files  = "HireplaceTok/**/*.{swift}"

  s.resources = "HireplaceTok/**/*.{png,jpeg,jpg,storyboard,xib,xcassets}"

  spec.dependency "OpenTok", "~> 2.15.3"
  spec.dependency "SnapKit", "~> 4.2.0"

  spec.swift_version = "4.2"
end
