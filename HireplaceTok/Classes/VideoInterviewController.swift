//
//  VideoInterviewController.swift
//  HireplaceTok
//
//  Created by Mike Tran on 5/4/19.
//  Copyright © 2019 Hireplace. All rights reserved.
//

import UIKit
import OpenTok
import SnapKit

private final class BundleToken {}

open class VideoInterviewController: UIViewController {
  
  public static func configureWith(apiKey: String, sessionId: String, token: String) -> VideoInterviewController {
    let identifier = "videoInterviewController"
    let storyboard = UIStoryboard(name: "Main", bundle:  Bundle(for: BundleToken.self))
    guard let controller = storyboard.instantiateViewController(withIdentifier: identifier) as? VideoInterviewController else {
      fatalError("ViewController '\(identifier)' is not of the expected class \(VideoInterviewController.self).")
    }
    controller.apiKey = apiKey
    controller.sessionId = sessionId
    controller.token = token
    
    return controller
  }
  
  open var onCloseTap: ((_ callDuration: TimeInterval) -> Void)?
  
  @IBOutlet private weak var closeButton: UIButton!
  @IBOutlet private weak var muteMicButton: UIButton!
  
  private var apiKey: String!
  fileprivate var sessionId: String!
  fileprivate var token: String!
  private var subscriber: OTSubscriber?
  private var error: OTError?
  
  private lazy var session: OTSession = {
    return OTSession(apiKey: apiKey, sessionId: sessionId, delegate: self)!
  }()
  
  private lazy var publisher: OTPublisher = {
    let settings = OTPublisherSettings()
    settings.name = UIDevice.current.name
    return OTPublisher(delegate: self, settings: settings)!
  }()
  
  override open func viewDidLoad() {
    super.viewDidLoad()
    
    self.view.backgroundColor = .white
    
    [closeButton, muteMicButton].forEach(self.view.addSubview)
    
    closeButton.snp.makeConstraints { make in
      make.top.equalTo(self.view.safeAreaLayoutGuide.snp.top).offset(10)
      make.trailing.equalToSuperview().inset(10)
      make.width.height.equalTo(24)
    }
    
    muteMicButton.snp.makeConstraints { make in
      make.leading.equalToSuperview().offset(20)
      make.bottom.equalToSuperview().inset(20)
    }
    
    doConnect()
  }
  
  override open func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }
}

extension VideoInterviewController {
  @IBAction private func muteMicAction(_ sender: AnyObject) {
    publisher.publishAudio = !publisher.publishAudio
    
    let buttonImage: UIImage  = {
      if !publisher.publishAudio {
        return #imageLiteral(resourceName: "mic_muted-24")
      } else {
        return #imageLiteral(resourceName: "mic-24")
      }
    }()
    
    muteMicButton.setImage(buttonImage, for: .normal)
  }
  
  @IBAction private func closeAction(_ sender: AnyObject) {
    let startTime = session.connection?.creationTime ?? Date()
    let endTime = Date()
    let duration = endTime.timeIntervalSince(startTime)
    session.disconnect(&error)
    onCloseTap?(duration)
    dismiss(animated: true, completion: nil)
  }
}

// MARK: - OpenTok Methods
extension VideoInterviewController {
  private func doConnect() {
    var error: OTError?
    defer {
      processError(error)
    }
    
    session.connect(withToken: self.token, error: &error)
  }
  
  private func doPublish() {
    muteMicButton.isEnabled = true
    
    var error: OTError?
    defer {
      processError(error)
    }
    
    session.publish(publisher, error: &error)
    
    if let pubView = publisher.view {
      view.addSubview(pubView)
      
      pubView.snp.makeConstraints { make in
        make.width.equalToSuperview().dividedBy(4)
        make.height.equalToSuperview().dividedBy(6)
        make.leading.equalToSuperview().offset(20)
        make.bottom.equalTo(self.view.safeAreaLayoutGuide.snp.bottom).inset(20)
      }
    }
  }
  
  private func doSubscribe(_ stream: OTStream) {
    var error: OTError?
    defer {
      processError(error)
    }
    subscriber = OTSubscriber(stream: stream, delegate: self)
    
    session.subscribe(subscriber!, error: &error)
  }
  
  fileprivate func cleanupSubscriber() {
    subscriber?.view?.removeFromSuperview()
    subscriber = nil
  }
  
  fileprivate func cleanupPublisher() {
    publisher.view?.removeFromSuperview()
  }
  
  private func processError(_ error: OTError?) {
    if let err = error {
      DispatchQueue.main.async {
        let controller = UIAlertController(title: "Error", message: err.localizedDescription, preferredStyle: .alert)
        controller.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(controller, animated: true, completion: nil)
      }
    }
  }
}

extension VideoInterviewController: OTSessionDelegate {
  public func sessionDidConnect(_ session: OTSession) {
    doPublish()
  }
  
  public func sessionDidDisconnect(_ session: OTSession) {
    
  }
  
  public func session(_ session: OTSession, didFailWithError error: OTError) {
    
  }
  
  public func session(_ session: OTSession, streamCreated stream: OTStream) {
    if subscriber == nil {
      doSubscribe(stream)
    }
  }
  
  public func session(_ session: OTSession, streamDestroyed stream: OTStream) {
    if let subStream = subscriber?.stream, subStream.streamId == stream.streamId {
      cleanupSubscriber()
    }
  }
}

extension VideoInterviewController: OTPublisherDelegate {
  public func publisher(_ publisher: OTPublisherKit, didFailWithError error: OTError) {
    
  }
  
  private func publisher(_ publisher: OTPublisherKit, streamDestroyed stream: OTStream) {
    cleanupPublisher()
    if let subStream = subscriber?.stream, subStream.streamId == stream.streamId {
      cleanupSubscriber()
    }
  }
  
}

extension VideoInterviewController: OTSubscriberDelegate {
  public func subscriberDidConnect(toStream subscriberKit: OTSubscriberKit) {
    if let subsView = subscriber?.view {
      view.insertSubview(subsView, belowSubview: closeButton)
      
      subsView.snp.makeConstraints { make in
        make.edges.equalToSuperview()
      }
    }
  }
  
  public func subscriber(_ subscriber: OTSubscriberKit, didFailWithError error: OTError) {
    
  }
  
}
